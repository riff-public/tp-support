import os 
import subprocess

# must change this path
ffmpeg_exe = "D:/pipeline/tp-support/src/ffmpeg/bin/ffmpeg.exe"
# ffmpeg_exe = "O:/Pipeline/tp-support/src/ffmpeg/bin/ffmpeg.exe"

def create_video_from_imgs(image_sequence_path, output_video_path, framerate=24):
    # Create the video from image sequence
    subprocess.call([
        ffmpeg_exe, '-framerate', str(framerate), '-i', image_sequence_path,
        '-c:v', 'libx264', '-pix_fmt', 'yuv420p', '-movflags', 'faststart', '-y', output_video_path
    ])
    return output_video_path


def combine_video_and_audio(video_file_path, audio_file_path, output_video_path, framerate=24): 
    # Combine the video with the audio
    subprocess.call([
        ffmpeg_exe, '-i', video_file_path, '-i', audio_file_path,
        '-c:v', 'copy', '-c:a', 'aac', '-strict', 'experimental', '-y', output_video_path
    ])
    return output_video_path


def create_video_imgs_audio(image_sequence_path, audio_file_path, output_video_path, framerate=24): 
	temp_video = '{}/temp.mp4'.format(os.path.dirname(output_video_path))
	temp_video = create_video_from_imgs(image_sequence_path, temp_video, framerate)
	combine_video_and_audio(temp_video, audio_file_path, outpythoput_video_path, framerate)
	os.remove(temp_video)
	return output_video_path

