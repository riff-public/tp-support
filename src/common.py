import subprocess


def run_bat_file(bat_file_path):
    """
    Runs a .bat file using the subprocess module.

    Args:
        bat_file_path (str): The path to the .bat file.
    """
    try:
        # Use subprocess.run to execute the .bat file
        result = subprocess.run([bat_file_path], check=True, capture_output=True, text=True)

        # Print the output and error (if any)
        print("Output:\n", result.stdout)
        print("Error (if any):\n", result.stderr)
    except subprocess.CalledProcessError as e:
        print(f"Error occurred: {e}")
        print("Return code:", e.returncode)
        print("Output:\n", e.output)
        print("Error:\n", e.stderr)
    
