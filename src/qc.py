import maya.cmds as mc 


def cleanup_wh(): 
    refs = mc.file(q=True, r=True)

    pipeline_path = 'TP/RawContent'
    forgive_nodes = ['persp', 'top', 'front', 'side']

    for ref in refs:
        if not pipeline_path in ref: 
            mc.file(ref, rr=True)
            
    dags = mc.ls(assemblies=True)
    for dag in dags: 
        is_ref = mc.referenceQuery(dag, inr=True)
        if not is_ref: 
            if not dag in forgive_nodes: 
                try: 
                    mc.delete(dag)
                except Exceptions as e: 
                    print(e)


def non_ref_namespace():
    """ list non-reference namespace """
    refs = mc.file(q=True, r=True)
    refNamespace = [mc.referenceQuery(a, namespace=True)[1:] for a in refs]
    allNamespaces = list_namespace()
    nonRefNamespaces = [a for a in allNamespaces if not a in refNamespace]
    return nonRefNamespaces


def list_namespace():
    """ list scene namespaces """
    excludeNs = ['UI', 'shared']
    namespaces = mc.namespaceInfo(listOnlyNamespaces=True, recurse=True)
    namespaces = [a for a in namespaces if not a in excludeNs] if namespaces else []
    return namespaces


def rename_duplicate_namespace_stacked():
    """ rename duplicate namespace for stacked namespace case """
    refs = mc.file(q=True, r=True)
    # refNamespace = [mc.referenceQuery(a, namespace=True).split(':')[-1] for a in refs]
    refDupPath = []
    refList = [] 
    listRefs = []
    for a in refs:
        refNs = mc.referenceQuery(a, namespace=True).split(':')[-1]
        # check for 1levels (standard) namespace
        if len(mc.referenceQuery(a, namespace=True).split(':')) <=2:
            refList.append(refNs)
        # check for objExists , namespace can't create with object exists
        elif mc.objExists(refNs):
            refList.append(refNs)
            listRefs.append(a)
        # lsit others refs
        else:
            listRefs.append(a)

    # loop check for duplicate namespace
    for a in listRefs:
        refNs = mc.referenceQuery(a, namespace=True).split(':')[-1]
        if refNs in refList:
            refDupPath.append(a)
        else:
            refList.append(refNs)
    #check version
    if refDupPath:
        for path in refDupPath:
            ns = mc.referenceQuery( path, namespace=True )
            parentNs = mc.referenceQuery( path, parentNamespace=True )
            if parentNs:
                if parentNs[0]:
                    mainNs = ns.split(':')[-1]
                    # suffix = re.findall('[0-9]+', mainNs)[-1]
                    oldNs = mainNs
                    while mainNs in refList or mc.objExists(mainNs):
                        version = ''
                        for num in mainNs[::-1]:
                            if num.isdigit():
                                version = num + version
                            else:
                                break
                        if version:
                            newVersion = int(version) + 1
                            newNs = mainNs.replace(version, '%03d'%newVersion)
                        else:
                            newNs = mainNs + '_001'
                        mainNs = newNs
                    mc.namespace(rename = ('%s:%s'%(parentNs[0],oldNs) , newNs), parent = parentNs[0])
                    print('Rename namespace {} to {} .'.format('%s:%s'%(parentNs[0],oldNs), '%s:%s'%(parentNs[0],newNs)))
                    refList.append(newNs)


def remove_namespaces2(namespaces):
    for namespace in reversed(namespaces):
        # mc.namespace(f=True, moveNamespace=(':%s' % namespace, ':'))
        mc.namespace(removeNamespace=namespace, mergeNamespaceWithRoot=True)


def clear_stacked_namespace_in_scene():
    """ clear all stacked namespace in scene just 1 level """
    namespaces = non_ref_namespace()
    rename_duplicate_namespace_stacked()
    remove_namespaces2(namespaces)


def run(): 
    cleanup_wh()
    clear_stacked_namespace_in_scene()
