from PySide2 import QtCore, QtWidgets, QtGui


class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        # main layouts
        self.all_layout = QtWidgets.QVBoxLayout()
        self.main_layout = QtWidgets.QVBoxLayout()
        self.cameraListWidget = QtWidgets.QListWidget()
        self.text_status = QtWidgets.QLabel('status : ')
        self.plasyblastButton = QtWidgets.QPushButton('playblast')

        self.main_layout.addWidget(self.cameraListWidget)
        self.main_layout.addWidget(self.text_status)
        self.main_layout.addWidget(self.plasyblastButton)
        self.all_layout.addLayout(self.main_layout)

        self.setLayout(self.all_layout)