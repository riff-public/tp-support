from shiboken2 import wrapInstance
from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui


from src.playblast import pb_ui
from src import common


_title = 'Playblast ALWH'
_version = 'v.1.0.0'
_des = ''
uiName = 'PlayblastUI'

from shiboken2 import wrapInstance
import maya.OpenMayaUI as mui

import maya.cmds as mc
import os
import shutil
import re

from src import ffmpeg_utils
from src import flow_utils


# import rf_config as config
# user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
# from rf_utils import log_utils
# logFile = log_utils.name(uiName, user)
# logger = log_utils.init_logger(logFile)

class PlayblastCore(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(PlayblastCore, self).__init__(parent)
        self._task_set = False

        # ui read
        self.ui = pb_ui.Ui(parent)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setObjectName(uiName)
        # self.setMinimumSize(QtCore.QSize(Config.ui_size[0], Config.ui_size[1]))
        # self.resize(Config.ui_size[0], Config.ui_size[1])

        # self.subclip_menu = QtWidgets.QMenu(self)

        # # --- add icons
        # # play movie button
        # play_iconWidget = QtGui.QIcon()
        # play_iconWidget.addPixmap(QtGui.QPixmap('{0}/play_icon_white.png'.format(icons_dir)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        # self.ui.play_pushButton.setIcon(play_iconWidget)


        # self.init_ui()
        self.init_functions()
        self.init_signals()

    def init_functions(self):
        self.get_all_camera()

    def init_signals(self):
        self.ui.plasyblastButton.clicked.connect(self.playblast_publish)

    def get_all_camera(self):
        exception_cams = ['frontShape', 'topShape', 'sideShape', 'perspShape']
        self.ui.cameraListWidget.clear()
        cameras = mc.ls(type='camera')
        camList = []
        for cam in cameras:
            if not cam in exception_cams: 
                camTf = mc.listRelatives(cam, p=True)[0]
                camList.append(camTf)

                item = QtWidgets.QListWidgetItem(camTf)
                item.setCheckState(QtCore.Qt.Checked)

                self.ui.cameraListWidget.addItem(item)
        return camList

    def get_selected_camera(self):

        items = []
        for x in range(self.ui.cameraListWidget.count()):
            item = self.ui.cameraListWidget.item(x)
            if item.checkState() == QtCore.Qt.Checked:
                items.append(item.text())
        return items

    def playblast_publish(self):
        scenepath = mc.file(q=True, sn=True)
        filename = os.path.basename(scenepath)
        raw_name, extension = os.path.splitext(filename)

        self.ui.text_status.setText('status : Playblasting . . .  Error Happened!!')
        self.ui.text_status.setStyleSheet('color: yellow;')
        cameras = self.get_selected_camera()
        for cam in cameras:
            mc.lookThru(cam)
            camname = cam.split(':')[-1]
            print('status : Playblasting {} please wait . . . '.format(cam))
            self.playblast('D:/PlayblastWH/{}/{}/{}'.format(raw_name, camname, raw_name),sequencer=0, format='image', codec='png', timeRange=True)

        pbPath = 'D:/PlayblastWH/{}'.format(raw_name)
        dstCombine = pbPath + '/combine'
        combineFile = dstCombine + '/combine_%04d.png'
        # dstMov = pbPath + '/mov/{}'.format(raw_name)
        # dstMov = pbPath + '/video'
        # if not os.path.exists(dstMov):
        #     os.makedirs(dstMov)
        
        new_name = raw_name
        ver = re.findall("[.-]*[vV][0-9]+", raw_name)
        if ver:
            new_name = raw_name.replace(ver[0],'')

        videoPath = pbPath + '/{}.mov'.format(new_name)
        self.combine_sequences(pbPath, dstCombine)
        self.convert_to_video(combineFile, videoPath)
        # self.upload(videoPath)
        self.cleanup(pbPath)

        self.ui.text_status.setText('status : Done. '.format(cam))
        self.ui.text_status.setStyleSheet('color: lightGreen;')

    def convert_to_video(self, image_sequence_path, output_video_path):
        ffmpeg_utils.create_video_from_imgs(image_sequence_path, output_video_path, framerate=30)

    def cleanup(self, path): 
        dirs = list_folder(path)

        for folder_path in dirs: 
            rmpath = '{}/{}'.format(path, folder_path)
            print('remove {}'.format(rmpath))
            shutil.rmtree(rmpath)

    def upload(self, movie_path):
        return 
        flow_utils.upload_from_name(movie_path)

    def combine_sequences(self, root_folders, dst_folder): 
        allow_ext = ['.png']
        ext = '.png'
        dirs = list_folder(root_folders)        
        i = 0
        for each in dirs: 
            if not each == 'combine': 
                path = '{}/{}'.format(root_folders, each)
                imgs = list_file(path)
                for img in imgs: 
                    if os.path.splitext(img)[-1] in allow_ext: 
                        newname = 'combine_%04d' % i
                        dstfile = '{}/{}{}'.format(dst_folder, newname, ext)
                        src = '{}/{}'.format(path, img)
                        
                        if not os.path.exists(os.path.dirname(dstfile)): 
                            os.makedirs(os.path.dirname(dstfile))
                        shutil.copy2(src, dstfile)
                        print('Copy {} -> {}'.format(src, dstfile))
                        i+=1

    def playblast(self, filepath, size=(1920, 1080), startFrame=1, endFrame=2, sequencer=1, format='qt', codec='H.264', timeRange=False, offScreen=True, quality=70):
        # format = 'image' or 'qt'
        # codec = 'H.264' or 'none'
        # compression = 'png'
        w, h = size
        audioFile = None
        useTraxSounds = 1
        if format == 'image':
            # filename no ext
            filepath, ext = os.path.splitext(filepath)
            useTraxSounds = 0

        if timeRange:
            startFrame = mc.playbackOptions(q=True, min=True)
            endFrame = mc.playbackOptions(q=True, max=True)

        result = mc.playblast( format=format,
                        # s=audioFile,
                        filename=filepath,
                        st=startFrame,
                        et=endFrame,
                        forceOverwrite=True,
                        sequenceTime=sequencer,
                        clearCache=1,
                        viewer=0,
                        showOrnaments=1,
                        fp=4,
                        widthHeight=[w,h],
                        percent=100,
                        useTraxSounds = useTraxSounds,
                        compression=codec,
                        offScreen=offScreen,
                        quality=quality
                        )

        return result

# def show():
#     from rftool.utils.ui import maya_win
#     logger.info('Run in Maya\n')
#     maya_win.deleteUI(uiName)
#     myApp = PlayblastCore(maya_win.getMayaWindow())
#     myApp.show()
#     return myApp

def list_file(path):
    files = []
    if os.path.exists(path): 
        files = [d for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]
    return files


def list_folder(path):
    dirs = []
    if os.path.exists(path): 
        dirs = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]
    return dirs 


def deleteUI(ui):
    if mc.window(ui, exists=True):
        mc.deleteUI(ui)
        deleteUI(ui)


def run_git_pull():
    repo_dir = 'D:/Pipeline/tp-support'
    try:
        # Change to the repository directory
        os.chdir(repo_dir)

        # Run the git pull command
        result = subprocess.run(['git', 'pull'], capture_output=True, text=True, check=True)

        # Print the output of the git pull command
        print("Output:\n", result.stdout)
        print("Error (if any):\n", result.stderr)
        print("Git pull completed successfully.")

    except subprocess.CalledProcessError as e:
        print(f"Error occurred: {e}")
        print("Git pull failed.")
    except Exception as e:
        print(f"Unexpected error: {e}")


def show():
    # run_git_pull()
    common.run_bat_file("D:\\Pipeline\\update.bat")
    ptr = mui.MQtUtil.mainWindow()
    deleteUI(uiName)
    myApp = PlayblastCore(wrapInstance(int(ptr), QtWidgets.QWidget))
    myApp.show()
    return myApp