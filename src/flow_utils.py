#!/usr/bin/python3.11
import os
import shotgun_api3 as shotgun

# Setup Shotgun API instance
SERVER_PATH = 'https://m2a.shotgunstudio.com'
SCRIPT_USER='whdf_vendor'
SCRIPT_KEY='uX9cexbtwncikolralprd^jek'
ca_certs = 'D:/pipeline/tp-support/shotgun_api3/lib/httplib2/python3/cacerts.txt'
SG = shotgun.Shotgun(SERVER_PATH, SCRIPT_USER, SCRIPT_KEY, ca_certs=ca_certs)

##################################
#Use SG as object to query or post

#Eg: finding all shots
#shots = SG.find("Shot", [], ["code","id"])
#for s in shots:
#    print (s)

#finding all tasks
#tasks = SG.find("Task", [], ["entity","id"])
#for t in tasks:
#    print (t)

#finding all versions
#versions = SG.find("Version", [], ["id", "entity"])
#for v in versions:
#    print (v)
##################################


# Example code, find the respective task to create a new version
# match the shot entity on the task with the name of the movie
# because there might be mismatch in lowercase / uppercase, we match
# after setting everthing to lowercase

# movie_path is the playblast file created locally


def upload_from_name2(movie_path): 
    # movie_path = "/path/to/movie/AS_SRM_fb_standTense_jumpBackStartled.mp4"
    movie_name = movie_path.split("/")[-1].split(".")[0]
    match = False

    #finding all tasks
    tasks = SG.find("Task", [], ["entity","id"])
    for t in tasks:
        if t['entity']:
            if (t['entity']['name']).lower() == movie_name.lower():
                print ("match:", t['entity']['id'])
                match = True

                #if there is a match, then we can use this entity for an upload
                data = { 'code'     : movie_name,
                        'project'   : {'type': 'Project', 'id': 2694, 'code': 'WDF'},
                        'sg_task'   : t,
                        'entity': t['entity'],
                        'sg_status_list'    : 'noaprv',
                        'description'   : 'uploaded from the riff'}

                versionEnt = SG.create('Version', data)
                result = SG.upload(versionEnt["type"], versionEnt["id"], movie_path,"sg_uploaded_movie")
                if result:
                    print ('version created : {0}'.format(movie_name))
    if not match: 
        print('Not found in ShotGrid {}'.format(movie_name))


def upload_from_name(movie_path): 
    movie_name = movie_path.split("/")[-1].split(".")[0]
    match = False
    shots = SG.find('Shot', [['project', 'is', {'type': 'Project', 'id': 2694, 'code': 'WDF'}]], ['code', 'id'])

    for shot in shots: 
        if shot['code'].lower() == movie_name.lower(): 
            match = True 
            print('Match {} with {}'.format(movie_name, shot))
            task = SG.find_one('Task', [['entity', 'is', shot], ['content', 'is', 'animation']], ['content', 'id'])
            data = {'code': movie_name,
                    'project': {'type': 'Project', 'id': 2694, 'code': 'WDF'},
                    'sg_task': task,
                    'entity': shot,
                    'sg_status_list': 'noaprv',
                    'description': 'uploaded from the riff'}

            version = SG.create('Version', data)
            result = SG.upload(version['type'], version['id'], movie_path, 'sg_uploaded_movie')
            return result
    print('{} not match any shot in ShotGrid'.format(movie_name))
